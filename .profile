export QT_QPA_PLATFORMTHEME="qt5ct"
export EDITOR=/usr/bin/vim
export GTK2_RC_FILES="$HOME/.gtkrc-2.0"

# fix "xdg-open fork-bomb" export your preferred browser from here
export BROWSER=/usr/bin/chromium

# set PATH for user custom scripts files
if [ -d "$HOME/bin" ]; then
    PATH="$HOME/bin:$PATH"
fi

# set PATH for user's private bin
if [ -d "$HOME/.local/bin:$PATH" ]; then
    PATH="$HOME/.local/bin:$PATH"
fi

# Postman App
if [ -d /opt/Postman/app ]; then
   PATH=$PATH:/opt/Postman/app
fi

# PostgreSql
alias joe-sandbox-rds='ssh -N -L 1234:sandbox-joe-web-server.cdvjcw31dpwp.eu-west-1.rds.amazonaws.com:5432 joe-sandbox-bastion'
alias joe-prod-rds='ssh -N -L 1235:production-joe-web-server.cdvjcw31dpwp.eu-west-1.rds.amazonaws.com:5432 joe-production-bastion'

# PostgreSql Aurora
alias joe-sandbox-aurora='ssh -N -L 1236:sandbox-joe-aurora-postgresql-1.cdvjcw31dpwp.eu-west-1.rds.amazonaws.com:5432 joe-sandbox-bastion'
alias joe-prod-aurora='ssh -N -L 1237:production-joe-aurora-postgresql.cluster-cdvjcw31dpwp.eu-west-1.rds.amazonaws.com:5432 joe-production-bastion'

